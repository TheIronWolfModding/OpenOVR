Wolf's Open Composite

To install:
* Copy openvr_api.dll into [rFactor 2 root]\Bin64 folder.  Usually [rFactor 2 root] is in Steam installation folder Steam\SteamApps\common\rFactor 2\Bin64\.
* If this is first time you install this .dll, optionally, copy included opencomposite.ini into the [rFactor 2 root]\Bin64 folder.  This file contains rF2 default settings for WOC.
* Adjust supersampleRatio value to your liking (this is Oculus PPD).

About this project:
https://gitlab.com/TheIronWolfModding/OpenOVR
